# Cuurency converter
## 2019

## Core

- [Typescript](https://www.typescriptlang.org/)

## Tools configured

- [TSLint](https://palantir.github.io/tslint/) - maintain javascript coding standards.
- [Emotion](https://emotion.sh/docs/introduction) - writing css styles with JavaScript.

## Libraries used

- [React](https://reactjs.org/)
- [React Router](https://reacttraining.com/react-router/)
- [Mobx](https://mobx.js.org/)
- [Parcel](https://parceljs.org/) - application bundler
