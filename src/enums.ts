export enum Breakpoints {
  XS = 'xs',
  SM = 'sm',
  MD = 'md',
  LG = 'lg',
  XL = 'xl'
}

export enum MessageType {
  ERROR = 'ERROR',
  SUCCESS = 'SUCCESS'
}
