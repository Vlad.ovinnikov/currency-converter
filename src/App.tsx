import * as React from 'react';
import * as H from 'history';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { observer } from 'mobx-react';
import { Global } from '@emotion/core';

import { LayoutRoute, MainLayout, IntroLayout } from './components/layout';
import { Wallet, Intro, Converter } from './components/pages';
import { AppStateComponent } from './appState/AppState';
import { globalStyles } from './styles';

interface IMatchParams {
  name: string;
  id: string;
}

export interface IRouteComponentProps<P> {
  match: IMatch<P>;
  location: H.Location;
  history: H.History;
  staticContext?: any;
}

export interface IMatch<P> {
  params: P;
  isExact: boolean;
  path: string;
  url: string;
}

export interface IAppProps extends RouteComponentProps<IMatchParams> {
}

@observer
class App extends AppStateComponent<IAppProps> {
  render() {
    return (
      <React.Fragment>
        <Global styles={ globalStyles } />
        <Switch>
          <IntroLayout
            path="/"
            component={ Intro }
            exact />
          <LayoutRoute
            path="/converter"
            layout={ MainLayout }
            component={ Converter }
            exact />
          <LayoutRoute
            path="/wallet"
            layout={ MainLayout }
            component={ Wallet }
            exact />
          <Route
            path="**"
            render={ () => <Redirect to="/" /> }
            exact />
        </Switch>
      </React.Fragment>
    );
  }
}

export default withRouter(App);
