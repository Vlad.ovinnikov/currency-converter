import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

import { AppStateComponent } from '../../../appState/AppState';

const Container = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 20px;
  text-align: center;
  background-color: lightgray;

  & a {
    display: inline-block;
    font-weight: 600;
    width: 100%;
    margin-top: 10px;
  }
`;

@observer
export default class extends AppStateComponent {

  render() {
    return (
      <Container>
        <h2>Wellcome</h2>
        <p>Currency Manager</p>
        <h4>Pages:</h4>
        <Link to="/converter">Converter</Link>
        <Link to="/wallet">Wallet</Link>
      </Container>
    );
  }
}
