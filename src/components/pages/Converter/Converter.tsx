import * as React from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { Link } from 'react-router-dom';
import moment from 'moment';
import {
  XAxis, YAxis, CartesianGrid, Tooltip, Legend, LineChart, Line
} from 'recharts';

import { AppStateComponent } from '../../../appState/AppState';
import { Input, Select, IOption, Button } from '../../common';
import Store, { IStore } from './FormStore';
import { FormGroup, Result } from './ConverterStyles';
import { DATE_FORMAT } from '../../../constants';

@observer
export default class extends AppStateComponent {

  @observable store: IStore = {} as IStore;
  @observable options: IOption[] = [] as IOption[];
  @observable firstDate = '2019-08 - 23';
  @observable secondDate = '';

  componentDidMount() {
    this.store = this.initStore({ amount: 0, base: '', target: 'EUR' });
    this.loadData();
    this.secondDate = this.substractDays(this.firstDate, 30);
  }

  handleInputChange = (ev: React.FormEvent<HTMLInputElement>) => {
    const { name, value } = ev.currentTarget;
    this.updateStore(name, value);
  }

  handleSelectChange = (ev: React.FormEvent<HTMLSelectElement>) => {
    const { name, value } = ev.currentTarget;
    this.updateStore(name, value);
  }

  handleSubmit = async (ev: React.FormEvent) => {
    ev.preventDefault();

    const { base, target, amount } = this.store;

    await this.appState.currency.convert(this.firstDate, base, target, amount);
    await this.appState.currency.historicalRates(this.secondDate, this.firstDate, target, base, amount);

    const { historicalResult } = this.appState.currency,
      p1 = historicalResult[0].y,
      p2 = historicalResult[historicalResult.length - 1].y;

    this.appState.currency.tendency(p1, p2);
  }

  render() {
    const { amount, base, target } = this.store,
      { convertationResult, historicalResult, tendencyResult } = this.appState.currency;

    return (
      <React.Fragment>
        <Link to={ '/' }>Intro</Link>
        <h3>Converter</h3>
        <h5>Date: { this.firstDate }</h5>
        <form onSubmit={ this.handleSubmit }>
          <FormGroup width="50%">
            <Select name="base"
              label="Base currency"
              value={ base }
              options={ this.options }
              onChange={ this.handleSelectChange }>
            </Select>
          </FormGroup>
          <FormGroup width="50%">
           Target: { target }
          </FormGroup>
          <FormGroup width="50%">
            <Input name="amount"
              label="Amount of money"
              placeholder="type amount here..."
              value={ amount }
              onChange={ this.handleInputChange } />
          </FormGroup>
          <FormGroup width="50%">
            <Button label="Get result" type="submit"/>
          </FormGroup>
        </form>
        { convertationResult && (
          <Result>
            <div style={ { marginBottom: '10px' } }>Result: { convertationResult } { target }</div>
            <div style={ { marginBottom: '10px' } }>Tendency: { tendencyResult }</div>
            <LineChart
              width={ 600 }
              height={ 300 }
              data={ historicalResult }
              margin={ {
                top: 5, right: 30, left: 20, bottom: 5,
              } }>
              <CartesianGrid strokeDasharray="5 5" />
              <XAxis dataKey="x" />
              <YAxis dataKey="y" />
              <Tooltip />
              <Legend />
              <Line type="monotone" dataKey="y" name="Amount, EUR" stroke="#8884d8" activeDot={ { r: 8 } } />
            </LineChart>
          </Result>
        ) }
     </React.Fragment>
    );
  }

  private readonly substractDays = (date: string, days: number) => {
    const result = new Date(date);
    result.setDate(result.getDate() - days);
    return moment(result).format(DATE_FORMAT);
  }

  private readonly initStore = ({ amount, base, target }: IStore) => {
    return new Store({
      amount: amount || 0,
      base: base || '',
      target: target || ''
    });
  }

  private updateStore(key: string, value: string) {
    this.store[key] = value;
  }

  private readonly loadData = async () => {
    this.appState.spinner.showPage();
    await this.appState.currency.getList();
    this.prepareOptions();
    this.appState.spinner.hidePage();
  }

  private readonly prepareOptions = async () => {
    const { symbols } = this.appState.currency;

    this.options = await Object.keys(symbols).reduce((accumulator: IOption[], currentItem: string): IOption[] => {
      accumulator.push({
        value: currentItem, label: `${ currentItem } - ${ symbols[currentItem] }`
      });
      return accumulator;
    }, [] as IOption[]);

    this.store.base = this.options[0].value.toString();
  }
}
