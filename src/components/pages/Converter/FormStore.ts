import { observable } from 'mobx';

export interface IStore {
  [key: string]: string | number;
  amount: number;
  base: string;
  target: string;
}

export default class Store {

  [key: string]: string |  number;
  @observable amount: number;
  @observable base: string;
  @observable target: string;

  constructor({ amount, base, target }: IStore) {
    this.amount = amount;
    this.base = base;
    this.target = target;
  }
}
