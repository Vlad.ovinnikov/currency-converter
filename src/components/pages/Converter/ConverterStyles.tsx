import styled from '@emotion/styled';

interface IFromGroup {
  width?: string;
}
export const FormGroup = styled.div<IFromGroup>`
  display: inline-block;
  width: ${ props => props.width ? props.width : 'auto' };
  padding: 0 10px;
  box-sizing: border-box;
`;

export const Result = styled.div`
  /* display: block; */
  color: #000;
  margin: 20px 10px;
  padding: 30px;
  background-color: #fae689;
  border: 1px solid darkgray;
  border-radius: 15px;
  box-sizing: border-box;
  text-align: center;
`;
