import * as React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import { AppStateComponent } from '../../../appState/AppState';

@observer
export default class extends AppStateComponent {

  render() {
    return (
      <React.Fragment>
        <Link to={ '/' }>Intro</Link>
        <h3>Wallet</h3>
     </React.Fragment>
    );
  }

}
