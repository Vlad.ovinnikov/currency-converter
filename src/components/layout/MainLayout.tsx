import * as React from 'react';
import { observer } from 'mobx-react';

import { Content, Footer, Header } from './index';
import { Breakpoints } from '../../enums';
import { AppStateComponent } from '../../appState/AppState';
import { IMatch } from '../../appState/RouterState';
import { withAppState } from '../../hocs';
import { Notification } from '../common';

interface IProps {
  match: IMatch;
  breakpoint: Breakpoints;
}

@observer
class MainLayout extends AppStateComponent<IProps> {

  render() {
    return (
      <main>
        <Header match={ this.props.match }
          { ...this.props } />
        <Content>
          { this.props.children }
        </Content>
        <Notification/>
        <Footer />
      </main>
    );
  }
}

export default withAppState(MainLayout);
