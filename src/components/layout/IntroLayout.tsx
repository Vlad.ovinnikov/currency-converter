import * as React from 'react';
import { Route } from 'react-router-dom';
import { observer } from 'mobx-react';

import { AppStateComponent } from '../../appState/AppState';

interface IProps {
  component: React.ElementType;
  path: string;
  exact?: boolean;
}

@observer
export default class extends AppStateComponent<IProps> {

  render() {
    const { component: Component, ...rest } = this.props;

    return (
      <Route render={ props => <Component { ...props } { ...rest} />} />
    );
  }
}
