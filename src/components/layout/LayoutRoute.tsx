import * as React from 'react';
import componentQueries from 'react-component-queries';

import { Route } from 'react-router-dom';
import { Breakpoints } from '../../enums';

interface IProps {
  component: React.ElementType;
  layout: React.ElementType;
}

const LayoutRoute = (layoutProps: IProps) => {
  const { component: Component, layout: Layout, ...rest } = layoutProps;

  return (
    <Route { ...rest }
      render={ props => (
          <Layout { ...layoutProps } { ...props }>
            <Component { ...props } />
          </Layout>
        ) } />
  );
},
  query = ({ width }: { width: number }) => {
    if (width < 575) {
      return { breakpoint: Breakpoints.XS };
    }

    if (width > 576 && width < 767) {
      return { breakpoint: Breakpoints.SM };
    }

    if (width > 768 && width < 991) {
      return { breakpoint: Breakpoints.MD };
    }

    if (width > 992 && width < 1199) {
      return { breakpoint: Breakpoints.LG };
    }

    if (width > 1200) {
      return { breakpoint: Breakpoints.XL };
    }

    return { breakpoint: Breakpoints.XS };
  },
  cqLayoutRoute = componentQueries(query)(LayoutRoute);

export default cqLayoutRoute;
