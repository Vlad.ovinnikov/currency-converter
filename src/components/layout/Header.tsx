import * as React from 'react';
import styled from '@emotion/styled';

const Header = styled.header`
  display: insline-block;
  color: lightgray;
  background-color: black;
  height: 60px;
  width: 100%;
`;

const BrandLogo = styled.div`
  width: 100%;
  padding: 20px;
`;

export default class extends React.PureComponent {

  render() {
    return (
      <Header>
        <BrandLogo>Currency converter</BrandLogo>
      </Header>
    );
  }
}
