import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';

import { AppStateComponent } from '../../appState/AppState';
import { Spinner } from '../common';

const Content = styled.div`
  padding: 15px;
  padding-bottom: 70px;
`;

@observer
export default class extends AppStateComponent {

  render() {
    const { ...restProps } = this.props,
      { isPageShow } = this.appState.spinner;

    return <Content { ...restProps } >
      <Spinner loading={ isPageShow } />
      { restProps.children }
    </Content>;
  }
}
