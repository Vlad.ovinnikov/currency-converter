import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';

import { AppStateComponent } from '../../../appState/AppState';

interface ILabel {
  htmlFor: string;
}
interface IProps {
  name: string;
  label: string;
  placeholder: string;
  value: string | number;
  type?: 'text' | 'password' | 'email';
  onChange(e: React.FormEvent<HTMLInputElement>): void;
}

const Wrapper = styled.div`
  margin-top: 15px;
`;

const Input = styled.input`
  font-size: 14px;
  background-color: #fff;
  padding: 7px 10px;
  margin-top: 5px;
  width: 100%;
  border: 1px solid #ced4da;
  background-clip: padding-box;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:focus {
    color: #495057;
    background-color: #fff;
    border-color: #404040;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 0, 0, 0.25);
  }
`;

const Label = styled.label<ILabel>`
  display: inline-block;
  width: 100%;
  font-size: 14px;
  font-weight: 600;
  padding-left: 5px;
  font-style: italic;
`;

@observer
export default class extends AppStateComponent<IProps> {

  onChange = (ev: React.FormEvent<HTMLInputElement>) => {
    this.props.onChange(ev);
  }

  render() {
    const { value, name, placeholder, label, type } = this.props;

    return (
      <Wrapper>
        <Label htmlFor={ name }>{ label }</Label>
        <Input id={ name }
          name={ name }
          type={ type || 'text' }
          placeholder={ placeholder }
          value={ value || '' }
          onChange={ this.onChange } />
      </Wrapper>
    );
  }
}
