import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';

import { AppStateComponent } from '../../../appState/AppState';

interface IProps {
  label: string;
  type?: 'button' | 'reset' | 'submit' | undefined;
  onClick?(): void;
}

const Wrapper = styled.div`
  margin-top: 15px;
`;

const Button = styled.button`
  color: #fff;
  font-size: 16px;
  background-color: #5eca62;
  padding: 10px 15px;
  margin-top: 5px;
  border: 1px solid #000;
  transition: background-color 0.3s;

  &:focus {
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 0, 0, 0);
  }
  &:hover {
    /* color: black; */
    background-color: #45b649;
    text-decoration: none;
  }
`;

@observer
export default class extends AppStateComponent<IProps> {

  render() {
    const { label, type } = this.props;

    return (
      <Wrapper>
        <Button id={ label }
          type={ type || 'button' }
          onClick={ this.props.onClick }>
          { label }
          </Button>
      </Wrapper>
    );
  }
}
