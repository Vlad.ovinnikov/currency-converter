import * as React from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { isString } from 'lodash';

import { AppStateComponent } from '../../../appState/AppState';

@observer
class Notification extends AppStateComponent {
  @observable.ref message = '';

  renderMessage() {
    if (this.appState.notification.successMessage && isString(this.appState.notification.successMessage)) {
      this.message = this.appState.notification.successMessage;
    }

    if (this.appState.notification.errorMessage && isString(this.appState.notification.errorMessage)) {
      this.message = this.appState.notification.errorMessage;
    }
  }

  render() {
    this.renderMessage();
    return (
      <div>{ this.message }</div>
    );
  }
}

export default Notification;
