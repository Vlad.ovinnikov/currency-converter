import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';

import { AppStateComponent } from '../../../appState/AppState';
import { observable } from 'mobx';

interface ILabel {
  htmlFor: string;
}

export interface IOption {
  value: string | number;
  label: string | number;
}

interface IProps {
  name: string;
  label: string;
  value: string;
  options: IOption[];
  onChange(e: React.FormEvent<HTMLSelectElement>): void;
}

const Wrapper = styled.div`
  margin-top: 15px;
`;

const Select = styled.select`
  display: block;
  color: #495057;
  line-height: 1.5;
  font-size: 14px;
  background-color: #fff;
  padding: 7px 10px;
  margin-top: 5px;
  width: 100%;
  border: 1px solid #ced4da;
  border-radius: 0;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

  &:focus {
    color: #495057;
    background-color: #fff;
    border-color: #404040;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 0, 0, 0.25);
  }
`;

const Label = styled.label<ILabel>`
  display: inline-block;
  width: 100%;
  font-size: 14px;
  font-weight: 600;
  padding-left: 5px;
  font-style: italic;
`;

@observer
export default class extends AppStateComponent<IProps> {

  @observable value = '';

  constructor(props: IProps) {
    super(props);
    this.value = props.value;
  }

  renderOptions = () => {
    const { options } = this.props;
    return options.map((option: IOption, key: number) => (
      <option key={ key } value={ option.value }>{ option.label }</option>
    ));
  }

  onChange = (ev: React.FormEvent<HTMLSelectElement>) => {
    this.props.onChange(ev);
  }

  render() {
    const { value, name, label } = this.props;

    return (
      <Wrapper>
        <Label htmlFor={ name }>{ label }</Label>
        <Select id={ name }
          name={ name }
          value={ value || '' }
          onChange={ this.onChange }>
          { this.renderOptions() }
        </Select>
      </Wrapper>
    );
  }
}
