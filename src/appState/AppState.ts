import * as React from 'react';
import { History } from 'history';

import SpinnerState from './SpinnerState';
import { history } from '../index';
import Axios, { CurrencyApi } from './services/apis';
import { CurrencyStateService } from './services';
import RouterStore from './RouterState';
import NotificationState from './NotificationState';

export class AppState {
  readonly spinner: SpinnerState;
  readonly history: History;
  readonly router: RouterStore;
  readonly currency: CurrencyStateService;
  readonly notification: NotificationState;

  constructor(historyState: History) {

    const router = new RouterStore(),
      notification = new NotificationState(),
      axios = new Axios(router),
      currencyApi = new CurrencyApi(axios);

    this.history = historyState;
    this.spinner = new SpinnerState();
    this.router = router;
    this.notification = notification;
    this.currency = new CurrencyStateService(currencyApi, notification);
  }
}

const AppContext = React.createContext(new AppState(history));

export const Provider = AppContext.Provider;

export class AppStateComponent<PropsType = {}, StateType = {}> extends React.Component<PropsType, StateType> {
  static contextType = AppContext;

  get appState(): AppState {
    return this.context;
  }
}
