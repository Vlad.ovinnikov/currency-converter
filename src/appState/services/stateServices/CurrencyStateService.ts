import { observable, action } from 'mobx';
import { isNumber } from 'lodash';

import { CurrencyApi } from '../apis';
import { ISymbol } from '../models';
import NotificationState from '../../NotificationState';
import { ERROR_API } from '../../messages';

interface IHistorical {
  [key: string]: { [key: string]: number };
}

interface IHistoricalTarget {
  x: string;
  y: number;
}

export default class {

  @observable.ref symbols: ISymbol = {} as ISymbol;
  @observable.ref convertationResult: number | null = null;
  @observable.ref historicalResult: IHistoricalTarget[] = [] as IHistoricalTarget[];
  @observable.ref tendencyResult: number | null = null;

  private readonly currencyApi: CurrencyApi;
  private readonly notification: NotificationState;

  constructor(currencyApi: CurrencyApi, notififcation: NotificationState) {
    this.currencyApi = currencyApi;
    this.notification = notififcation;
  }

  @action getList = async () => {
    try {
      const response = await this.currencyApi.list();

      if (!response.data.success) {
        this.notification.setErrorMessage(ERROR_API);
      }

      this.symbols = response.data.symbols;

      return response;

    } catch (error) {
      return error;
    }
  }

  @action convert = async (date: string, from: string, to: string, amount: number) => {
    console.log('date', date);
    console.log('from', from);
    console.log('to', to);
    console.log('amount', amount);

    try {

      const response = await this.currencyApi.latest(date, to);

      if (!response.data.success) {
        this.notification.setErrorMessage(ERROR_API);
      }

      const rates: { [key: string]: number } = response.data.rates;

      this.convertationResult = isNumber(rates[from]) ? parseFloat((amount / rates[from]).toFixed(2)) : 0;

      return response;

    } catch (error) {
      return error;
    }
  }

  @action historicalRates = async (startDate: string, endDate: string, base: string, target: string, amount: number) => {

    try {
      const response = await this.currencyApi.timeseries(startDate, endDate, base, target);

      if (!response.data.success) {
        this.notification.setErrorMessage(ERROR_API);
      }

      const rates: IHistorical = response.data.rates;

      this.historicalResult = await this.historicalDatamapper(target, rates, amount);

      return response;

    } catch (error) {
      return error;
    }
  }

  @action tendency = async (p1: number, p2: number) => {
    this.tendencyResult = parseFloat(((p2 - p1) / p1).toFixed(4));
  }

  private readonly historicalDatamapper = (target: string, rates: IHistorical, amount: number): IHistoricalTarget[] => {
    return Object.keys(rates).reduce((accumulator: IHistoricalTarget[], currentItem: string): IHistoricalTarget[] => {
      accumulator.push({
        x: currentItem, y: parseFloat((amount / rates[currentItem][target]).toFixed(2))
      });
      return accumulator;
    }, [] as IHistoricalTarget[]);
  }

}
