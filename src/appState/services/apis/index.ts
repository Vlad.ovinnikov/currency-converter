import axios from 'axios';

import { API_URL } from '../../../constants';
import CurrencyApi from './CurrencyApi';
import RouterStore from '../../RouterState';

export {
  CurrencyApi
};

interface IHeaders {
  'Content-Type': string;
  Authorization?: string;
}
export default class Axios {

  readonly router: RouterStore;

  constructor(router: RouterStore) {
    this.router = router;
  }

  apiJson() {
    const headers: IHeaders = {
      'Content-Type': 'application/json'
    };

    const instance = axios.create({
      headers,
      crossDomain: true,
      baseURL: `${ API_URL }`,
      processData: false,
    });

    this.responseInterceptor(instance);
    return instance;
  }

  apiForm() {
    const instance = axios.create({
      baseURL: `${ API_URL }`,
      async: true,
      crossDomain: true,
      headers: {
      },
      processData: false,
      contentType: false,
      mimeType: 'multipart/form-data',
    });

    this.responseInterceptor(instance);
    return instance;
  }

  private responseInterceptor(instance: any) {
    return instance.interceptors.response.use((response: any) => {
      return Promise.resolve(response);

    }, (error: any) => {
      if (!error.response) {
        return Promise.reject('Something went wrong with connection');
      }

      return Promise.reject(error.response);
    });
  }
}
