import Axios from './index';
import { ACCESS_KEY } from '../../../constants';

export default class {

  private readonly axios: Axios;

  constructor(axios: Axios) {
    this.axios = axios;
  }

  list() {
    // return this.axios.apiJson().get(`/symbols?access_key=${ ACCESS_KEY }`);
    return {
      data: {
        success: true,
        symbols: {
          CAD: 'Canadian Dollar',
          GBP: 'British Pound Sterling',
          USD: 'United States Dollar'
        }
      }
    };
  }

  latest(date: string, to: string) {
    console.log('date', date);
    console.log('to', to);

    // return this.axios.apiJson().get(`/${ date }?access_key=${ ACCESS_KEY }&base=${ to }`);
    return {
      data: {
        success: true,
        historical: true,
        date: '2013-12-24',
        timestamp: 1387929599,
        base: 'EUR',
        rates: {
          GBP: 0.915499,
          CAD: 1.49864,
          USD: 1.12405
        }
      }
    };
  }

  timeseries(startDate: string, endDate: string, base: string, target: string) {
    console.log('startDate', startDate);
    console.log('endDate', endDate);
    console.log('base', base);
    console.log('target', target);

    // Works API with subscription
    // return this.axios.apiJson()
    //   .get(`/timeseries?access_key=${ ACCESS_KEY }&start_date=${ startDate }&end_date=${ endDate }&base=${ base }&target=${ target }`);

    return {
      data: {
        success: true,
        timeseries: true,
        start_date: '2019-08-23',
        end_date: '2019-07-24',
        base: 'EUR',
        rates: {
          '2019-08-23': { USD: 1.332891, GBP: 0.912499, CAD: 1.59364 },
          '2019-08-22': { USD: 1.315066, GBP: 0.911489, CAD: 1.39371 },
          '2019-08-21': { USD: 1.344491, GBP: 0.910481, CAD: 1.59671 },
          '2019-08-20': { USD: 1.324481, GBP: 0.925482, CAD: 1.59172 },
          '2019-08-19': { USD: 1.324471, GBP: 0.918483, CAD: 1.59271 },
          '2019-08-18': { USD: 1.354696, GBP: 0.91948, CAD: 1.59268 },
          '2019-08-17': { USD: 1.354491, GBP: 0.916482, CAD: 1.39367 },
          '2019-08-16': { USD: 1.344891, GBP: 0.911485, CAD: 1.29369 },
          '2019-08-13': { USD: 1.343493, GBP: 0.912485, CAD: 1.29366 },
          '2019-08-12': { USD: 1.334591, GBP: 0.935486, CAD: 1.29367 },
          '2019-08-11': { USD: 1.314593, GBP: 0.945486, CAD: 1.29966 },
          '2019-08-10': { USD: 1.372292, GBP: 0.935485, CAD: 1.29966 },
          '2019-08-09': { USD: 1.362392, GBP: 0.925487, CAD: 1.39967 },
          '2019-08-08': { USD: 1.363792, GBP: 0.925488, CAD: 1.39968 },
          '2019-08-07': { USD: 1.354176, GBP: 0.965486, CAD: 1.39571 },
          '2019-08-06': { USD: 1.355292, GBP: 0.914486, CAD: 1.49572 },
          '2019-08-05': { USD: 1.325367, GBP: 0.917488, CAD: 1.49171 },
          '2019-08-04': { USD: 1.304897, GBP: 0.918491, CAD: 1.4917 },
          '2019-08-03': { USD: 1.325588, GBP: 0.916492, CAD: 1.69169 },
          '2019-08-02': { USD: 1.324457, GBP: 0.914492, CAD: 1.69368 },
          '2019-08-01': { USD: 1.334757, GBP: 0.914492, CAD: 1.69367 },
          '2019-07-31': { USD: 1.335277, GBP: 0.914491, CAD: 1.69368 },
          '2019-07-30': { USD: 1.355399, GBP: 0.913493, CAD: 1.69968 },
          '2019-07-29': { USD: 1.345294, GBP: 0.913493, CAD: 1.49967 },
          '2019-07-28': { USD: 1.355433, GBP: 0.911492, CAD: 1.49965 },
          '2019-07-27': { USD: 1.365456, GBP: 0.911492, CAD: 1.49864 },
          '2019-07-26': { USD: 1.375464, GBP: 0.911492, CAD: 1.49864 },
          '2019-07-25': { USD: 1.365448, GBP: 0.911494, CAD: 1.49866 },
          '2019-07-24': { USD: 1.355492, GBP: 0.914492, CAD: 1.49865 },
        }
      }
    };
  }

}
