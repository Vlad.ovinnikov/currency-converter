export default interface ISymbol {
  [key: string]: string;
}
