import { observable, action } from 'mobx';

export default class SpinnertState {

  @observable isPageShow = false;

  @action showPage() {
    this.isPageShow = true;
  }

  @action hidePage() {
    setTimeout(() => {
      this.isPageShow = false;
    }, 500);
  }
}
