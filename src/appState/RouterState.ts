import { observable, action } from 'mobx';
import { History } from 'history';

interface ILocation {
  hash: string;
  key: string;
  pathname: string;
  search: string;
  state: string | never;
}

export interface IMatch {
  isExact: boolean;
  params: { [key1: string]: string };
  path: string;
  url: string;
}

class RouterStore {

  @observable location: ILocation = {} as ILocation;
  @observable match: IMatch = {} as IMatch;
  @observable history: History = {} as History;

  @action setRoute(location: ILocation, match: IMatch, history: History) {
    this.location = location;
    this.match = match;
    this.history = history;
  }

  @action redirectTo(path: string) {
    this.history.push(path);
  }
}

export default RouterStore;
