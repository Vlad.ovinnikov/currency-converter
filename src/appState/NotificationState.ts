import { observable, action } from 'mobx';

class NotificationState {
  @observable errorMessage: string | null = null;
  @observable successMessage: string | null = null;

  @action setErrorMessage(errorMessage: string) {

    this.errorMessage = errorMessage;

    console.error(this.errorMessage);

    const timeout = window.setTimeout(() => {
      this.errorMessage = null;
      clearTimeout(timeout);
    }, 2000);
  }

  @action setSuccessMessage(successMessage: string) {
    this.successMessage = successMessage;

    const timeout = window.setTimeout(() => {
      this.successMessage = null;
      clearTimeout(timeout);
    }, 2000);
  }
}

export default NotificationState;
