import * as React from 'react';
import { AppStateComponent } from '../appState/AppState';
import { observer } from 'mobx-react';

export default (WrappedComponent: React.ElementType): React.ElementType => {
  @observer
  class WithAppState extends AppStateComponent<any> {

    componentWillMount() {
      this.appState.router.setRoute(this.props.location, this.props.match, this.props.history);
    }

    render() {
      return <WrappedComponent { ...this.props } />;
    }
  }

  return WithAppState;
};
