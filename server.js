/**
 * Created by: Jax
 * Date: 2019-02-08
 * Time: 18:56
 */
const express = require('express'),
    app = express(),
    path = require('path');

// Run the app by serving the static files
// in the dist directory
app.use(express.static(__dirname + './../build'));

// Start the app by listening on the default
app.listen(process.env.PORT || 3001, err => {
    if (err) {
        console.error(err);
    } else {
        console.info(`Listening on PORT: ${process.env.PORT || 3001}`);
    }
});

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname + './../build/index.html'));
});
